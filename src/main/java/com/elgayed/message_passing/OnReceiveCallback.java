package com.elgayed.message_passing;

/**
 * Implementations of this interface are passed to {@link LocalPlayer#onReceiveCallback} to be invoked when new messages are received
 */
public interface OnReceiveCallback {
	
	/**
	 * Message received callback method
	 * @param message the received message
	 * @param from sender
	 * @param to receiver
	 * @param receiverSentMessagesCount Number of messages sent by the receiver
	 */
	public void onReceive (String message, Player from, Player to, int receiverSentMessagesCount);
}
