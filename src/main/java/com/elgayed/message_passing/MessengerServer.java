package com.elgayed.message_passing;

/**
 * MessengerServer interface acts as mediator providing a contract to handle interaction between {@code Player} instances
 */
public interface MessengerServer<T extends Player> {
	
	/**
	 * Handles sending messages between two {@code Player} instances.
	 * @param message content to be sent
	 * @param sender the player sending the message
	 * @param receiver the player receiving the message
	 */
	public void sendMessage(String message, T sender, T receiver);
}
