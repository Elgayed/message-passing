package com.elgayed.message_passing;

public class Main {

	public static void main(String[] args) {
		
		MessengerServer<LocalPlayer> messenger = new LocalMessenger();
		
		LocalPlayer initiator = new LocalPlayer("initiator", messenger);
		LocalPlayer receiver = new LocalPlayer("receiver", messenger);
		
		initiator.setOnReceiveCallback(
			// initiator OnReceiveCallback implementation as a lamdba, alternatively this logic goes to InitiatorOnReceiveCallback class implementing OnReceiveCallback
			// bounces back message concatenated with message count + checking for stop condition
			(recievedMessage, from, to, count) -> {
				// stop condition
				if (count == 10) {
					System.exit(0);
				}
				to.send(recievedMessage + count, from);
			}
		);
		
		receiver.setOnReceiveCallback(
			// receiver OnReceiveCallback implementation as a lamdba, alternatively this logic goes to ReceiverOnReceiveCallback class implementing OnReceiveCallback
			// bounces back message concatenated with message count
			(recievedMessage, from, to, count) -> {
				to.send(recievedMessage + count, from);
			}
		);
		
		initiator.send("Hello from initiator ", receiver);
	}

}
