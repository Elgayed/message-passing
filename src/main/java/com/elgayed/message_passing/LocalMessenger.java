package com.elgayed.message_passing;

/**
 * Provide implementation for sending messages between local players (Player instances on the same java process)
 */
public class LocalMessenger implements MessengerServer<LocalPlayer> {
	
	/**
	 * {@inheritDoc}
	 * <br> This implementation is responsible for sending messages between {@code LocalPlayer} instances via direct feature invocation (by invoking {@link LocalPlayer#receive(String, LocalPlayer)} method) 
	 */
	@Override
	public void sendMessage(String message, LocalPlayer sender, LocalPlayer receiver) {
		receiver.receive(message, sender);
	}
}
