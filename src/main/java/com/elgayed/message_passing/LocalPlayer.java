package com.elgayed.message_passing;

import java.util.Optional;
import java.util.logging.Logger;

/**
 * Implementation for local Player instances (living on the same java process)
 */
public class LocalPlayer extends Player{
	

	private static final Logger LOGGER = Logger.getLogger(LocalPlayer.class.getCanonicalName());
	private Integer messagesSentCounter = 0;
	
	protected Optional<OnReceiveCallback> onReceiveCallback;
	
	public LocalPlayer(String name, MessengerServer<LocalPlayer> messenger) {
		super(name, messenger);
		this.onReceiveCallback = Optional.ofNullable(null);
	}
	
	public LocalPlayer(String name, MessengerServer<LocalPlayer> messenger, OnReceiveCallback onReceiveCallback) {
		this(name, messenger);
		this.onReceiveCallback = Optional.ofNullable(onReceiveCallback);
	}
	
	/**
	 * @param onReceiveCallback Implementation for callback that will be invoked when messages are received
	 */
	public void setOnReceiveCallback(OnReceiveCallback onReceiveCallback) {
		this.onReceiveCallback = Optional.ofNullable(onReceiveCallback);
	}
	
	
	/**
	 * {@inheritDoc}
	 * <br>
	 * Increments an internal counter keeping track of messages sent by {@code this}
	 */
	@Override
	protected void send(String message, Player to) {
		this.messagesSentCounter ++;
		LOGGER.info(String.format("Player '%s': sending message '%s', to player '%s'", this.getName(), message, to.getName()));
		this.messenger.sendMessage(message, this, to);
	}
	
	/**
	 * {@inheritDoc}
	 * <br>If {@link #onReceiveCallback} is present, its implementation will be invoked when messages are received 
	 */
	@Override
	protected void receive(String message, Player from) {
		LOGGER.info(String.format("Player '%s': received message '%s', from player '%s'", this.getName(), message, from.getName()));
		onReceiveCallback.ifPresent(onReceiveCallback -> onReceiveCallback.onReceive(message, from, this, this.messagesSentCounter.intValue()));
	}
}
