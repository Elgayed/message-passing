package com.elgayed.message_passing;

/**
 * Player base class
 */
public abstract class Player {
	
	protected final String name;
	protected final MessengerServer messenger;
	
	public Player(final String name, final MessengerServer messenger) {
		this.name = name;
		this.messenger = messenger;
	}
	
	public String getName() {
		return name;
	}

	public MessengerServer getMessengerServer() {
		return messenger;
	}
	
	/**
	 * Sends a message to another player instance
	 * @param message to send
	 * @param to receiver {@code Player}
	 */
	protected abstract void send (String message, Player to);
	
	/**
	 * Receives a message from another player instance
	 * @param message content to be received
	 * @param from sender {@code Player}
	 */
	protected abstract void receive (String message, Player from);
}
