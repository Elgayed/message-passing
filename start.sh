#!/bin/bash

# ------------------------------------------------------------------
# Title: START SCRIPT
# Description: Utility script for packaging and starting this maven project
# ------------------------------------------------------------------

echo "Building application"
./mvnw clean package
echo "Running application"
java -jar ./target/message-passing-0.0.1-SNAPSHOT-jar-with-dependencies.jar
